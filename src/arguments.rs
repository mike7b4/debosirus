use std::collections::HashMap;
use std::error::Error;
use std::path::PathBuf;
use structopt::StructOpt;
/// Parse a single key-value pair
fn parse_key_val<T, U>(s: &str) -> Result<(T, U), Box<dyn Error>>
where
    T: std::str::FromStr,
    T::Err: Error + 'static,
    U: std::str::FromStr,
    U::Err: Error + 'static,
{
    let pos = s
        .find('=')
        .ok_or_else(|| format!("invalid KEY=value: no `=` found in `{}`", s))?;
    Ok((s[..pos].parse()?, s[pos + 1..].parse()?))
}

#[derive(StructOpt, Debug)]
#[structopt(name = "debosirus", version = "0.0.1")]
pub struct Arguments {
    #[structopt(short = "t", long = "rootdir", parse(from_os_str))]
    pub rootdir: PathBuf,
    #[structopt(short = "r", long = "recipe", parse(from_os_str), help = "yaml recipe")]
    pub config: PathBuf,
    #[structopt(short = "s", long = "skip", help = "Skip action")]
    pub skip_actions: Vec<String>,
    #[structopt(short = "D", long = "define", parse(try_from_str = parse_key_val), help="key=value for example -D email=foo@bar.com")]
    defines: Vec<(String, String)>,
    // after "auto" parse -D we move it to hashmap.
    #[structopt(skip)]
    pub map: HashMap<String, String>,
}

impl Arguments {
    pub fn new() -> Result<Self, crate::errors::Error> {
        let mut args = Arguments::from_args();
        if args.rootdir.is_absolute() {
            return Err(crate::Error::AbsolutePathNotAllowed(args.rootdir.clone()));
        }
        // Kind of quick and dirty, I could not find a way to
        // parse it and put it in a HashMap directly :/
        // At least this doesn't copy the string itself...
        //        if let Some(defines) = args.defines.take() {
        for (key, value) in &args.defines {
            args.map.insert(key.clone(), value.clone());
        }
        //       }

        let mut skipped: Vec<String> = vec![];
        for actions in args.skip_actions {
            for action in actions.split(',') {
                skipped.push(action.to_string());
            }
        }
        args.skip_actions = skipped;

        Ok(args)
    }

    pub fn is_action_skipped(&self, action: &str) -> bool {
        for skip_action in &self.skip_actions {
            if skip_action == action {
                return true;
            }
        }
        false
    }
}
