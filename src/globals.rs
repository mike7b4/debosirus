use crate::actions::chroot::ChRoot;
use crate::arguments::Arguments;
lazy_static! {
    pub static ref ARGS: Arguments = {
        Arguments::new()
            .map_err(|e| {
                eprintln!("{}", e);
                std::process::exit(1);
            })
            .unwrap()
    };
    pub static ref CHROOT: ChRoot = { ChRoot::new(&ARGS.rootdir) };
}
