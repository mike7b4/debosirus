use super::runner::Runner;
use crate::actions::inject::Inject;
use crate::errors::Error;
use crate::globals::{ARGS, CHROOT};
use serde::Deserialize;
use serde_yaml::Value;
use std::convert::TryFrom;
use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;
#[derive(Deserialize, Debug)]
pub struct User {
    uid: usize,
    login: String,
    #[serde(default = "def_false")]
    create_git_config: bool,
    #[serde(default = "def_false")]
    create_ssh_key: bool,
    #[serde(default = "def_false")]
    install_rust: bool,
    rust_target: Option<String>,
    ssh_config: Option<String>,
    #[serde(skip)]
    home_path: PathBuf,
}

fn def_false() -> bool {
    false
}

impl TryFrom<&Value> for User {
    type Error = Error;
    fn try_from(value: &Value) -> Result<Self, Error> {
        let user: User = serde_yaml::from_value(value.clone()).map_err(Error::Serde)?;
        if user.create_git_config || user.create_ssh_key {
            ARGS.map.get("email").ok_or(Error::MissingDefine("email"))?;
            ARGS.map
                .get("fullname")
                .ok_or(Error::MissingDefine("fullname"))?;
        }

        Ok(user)
    }
}

impl Runner for User {
    fn run(&mut self) -> Result<(), Error> {
        self.home_path = std::path::PathBuf::from(&ARGS.rootdir);
        self.home_path.push("home");
        self.home_path.push(&self.login);

        self.add_user()?;
        if self.create_git_config {
            self.create_git_config()?;
        }
        if self.create_ssh_key {
            self.create_ssh_key()?;
        }
        self.create_ssh_config()?;
        // If we come here we make sure drop does not run deluser
        Ok(())
    }
}

impl User {
    fn add_user(&mut self) -> Result<(), Error> {
        let mut cmd = String::from("useradd ");
        self.uid = ARGS
            .map
            .get("uid")
            .ok_or(Error::MissingDefine("uid"))?
            .parse()
            .unwrap_or(self.uid);
        if self.uid == 0 {
            return Err(Error::MissingDefine("User ID must not be > 0"));
        }
        cmd += &format!("--uid {} ", self.uid);
        cmd += &format!("{} --create-home --shell /bin/bash", self.login);
        CHROOT.run(cmd)
    }

    fn create_git_config(&mut self) -> Result<(), Error> {
        let email = ARGS.map.get("email").ok_or(Error::MissingDefine("email"))?;
        let full = ARGS
            .map
            .get("fullname")
            .ok_or(Error::MissingDefine("fullname"))?;
        let gitconfig = format!(
            r#"[user]
name = {}
email = {}"#,
            full, email
        );
        let mut path = self.home_path.clone();
        path.push(".gitconfig");
        File::create(path)?
            .write_all(gitconfig.as_bytes()).map_err(Error::IO)
    }

    fn create_ssh_key(&mut self) -> Result<(), Error> {
        let email = ARGS.map.get("email").ok_or(Error::MissingDefine("email"))?;
        CHROOT.run_as(
            &self.login,
            format!("mkdir -p -m 0700 /home/{}/.ssh", &self.login),
        )?;
        CHROOT.run_as(
            &self.login,
            format!(
                "/usr/bin/ssh-keygen -q -N \"\" -t rsa -b 4096 -C {} -f /home/{}/.ssh/id_rsa",
                &email, self.login
            ),
        )?;
        Ok(())
    }

    fn create_ssh_config(&mut self) -> Result<(), Error> {
        if let Some(ssh_config) = self.ssh_config.take() {
            let mut path = self.home_path.clone();
            path.push(".ssh");
            // We don't care if create fails, probably because already created.
            std::fs::create_dir(&path).unwrap_or(());
            // Inject expect relative path
            let mut conffile = PathBuf::from("home");
            conffile.push(&self.login);
            conffile.push(".ssh/config");
            let mut inject = Inject::new(conffile, 0o600, ssh_config);
            inject.run()?;
        }

        Ok(())
    }
}

impl Drop for User {
    fn drop(&mut self) {
        //    CHROOT.run(format!("deluser --quiet --remove-home {}", self.login)).unwrap_or(());
    }
}
