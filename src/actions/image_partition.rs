use super::runner::Runner;
use crate::actions::sudo::Sudo;
use crate::errors::Error;
use crate::losetup::Losetup;
use libc;
use serde::Deserialize;
use serde_humanize_rs;
use serde_yaml::Value;
use std::convert::TryFrom;
use std::fmt;
use std::fs::{remove_file, File};
use std::ops::Add;
use std::os::unix::io::AsRawFd;
use std::process::{Command, Stdio};

#[derive(Deserialize, Debug)]
struct Mountpoint {
    mountpoint: String,
    partition: String,
}

#[derive(Debug, PartialEq, Clone)]
pub enum MySize {
    Size(usize),
    Percent(String),
}

impl From<&MySize> for usize {
    fn from(value: &MySize) -> Self {
        match value {
            MySize::Size(value) => *value,
            MySize::Percent(value) => panic!(format!(
                "FIXME translation from MySize::Percent({}) to usize is not implemented",
                value
            )),
        }
    }
}

impl Add for MySize {
    type Output = usize;
    fn add(self, other: MySize) -> usize {
        usize::from(&self) + usize::from(&other)
    }
}

struct Parted {
    imagename: String,
}

impl Parted {
    pub fn new(imagename: &str) -> Self {
        Self {
            imagename: imagename.into(),
        }
    }

    fn create_label(&self, kind: &str) -> Result<(), Error> {
        let mut args: Vec<String> = vec![];
        args.push(String::from("mklabel"));
        args.push(String::from(kind));
        self.run(args).map_err(|_e| Error::MkLabel)
    }

    fn run(&self, args: Vec<String>) -> Result<(), Error> {
        let mut cmd = Command::new("parted")
            .arg(String::from("-s"))
            .arg(self.imagename.clone())
            .args(args)
            .stdin(Stdio::piped())
            .spawn()?;

        let status = cmd.wait()?;
        // Note Command::spawn may fail to execute in that case code is None
        // see std manual...
        // ok_or translate None to our error enum.
        let code = status.code().ok_or(Error::CommandExecuteFailed)?;
        if code != 0 {
            return Err(Error::CommandFailed(String::from("parted failed"), code));
        }
        Ok(())
    }
}

#[derive(Deserialize, Debug, Clone)]
pub struct Partition {
    name: String,
    fs: String,
    #[serde(with = "serde_humanize_rs")]
    start: usize,
    #[serde(with = "de_size")]
    size: MySize,
    #[serde(default = "default_empty_string_vec")]
    flags: Vec<String>,
}

impl Partition {
    pub fn create(&self, imagename: &str) -> Result<(), Error> {
        let mut args: Vec<String> = vec![];
        args.push("mkpart".into());
        args.push("primary".into());
        args.push(self.fs.to_string());
        args.push(format!("{}B", self.start));
        args.push(match &self.size {
            MySize::Size(value) => format!("{}B", (self.start + value).to_string()),
            MySize::Percent(value) => value.clone(),
        });
        println!("{:?}", args);
        Parted::new(imagename).run(args)?;
        Ok(())
    }

    pub fn lookup_mkfs_backend(&self, fs: &str) -> Result<&str, Error> {
        match fs {
            "fat16" => Ok(&"mkfs.msdos"),
            "msdos" => Ok(&"mkfs.msdos"),
            "ext4" => Ok(&"mkfs.ext4"),
            "btrfs" => Ok(&"mkfs.btrfs"),
            _ => Err(Error::NotImplemented(format!("mkfs backend for {}", fs))),
        }
    }

    pub fn format_disk(&self, imagename: &str) -> Result<(), Error> {
        let lo = Losetup::attach(imagename, self.start)?;
        let mkfs = self.lookup_mkfs_backend(&self.fs.as_str())?;
        println!("Format '{}' with '{}'", &lo.path, mkfs);
        let mut args = Vec::new();
        args.push(String::from(mkfs));
        args.push(lo.path.clone());
        Sudo::new()
            .run(args)
            .unwrap_or_else(|e| panic!("{} on {}", e, &self.fs));
        Ok(())
    }

    pub fn run(&mut self, imagename: &str) -> Result<(), Error> {
        self.create(imagename)?;
        self.format_disk(imagename)?;
        Ok(())
    }
}

#[derive(Deserialize, Debug)]
pub struct ImagePartition {
    imagename: String,
    #[serde(with = "serde_humanize_rs")]
    imagesize: usize,
    partitiontype: String,
    #[serde(default = "default_gptgap")]
    gptgap: String,
    mountpoints: Vec<Mountpoint>,
    partitions: Vec<Partition>,
}

mod de_size {
    use crate::actions::image_partition::MySize;
    use humanize_rs::bytes::Bytes;
    use serde::de::Unexpected;
    use serde::{Deserialize, Deserializer};
    use std::str::FromStr;
    pub fn deserialize<'de, D>(deserializer: D) -> Result<MySize, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        println!("{:?}", &s);
        match Bytes::from_str(&s) {
            Ok(value) => Ok(MySize::Size(value.size())),
            Err(_) => {
                let re = regex::Regex::new("^([0-9]){1,2}([0]){0,1}%$").unwrap();
                if re.is_match(&s) {
                    return Ok(MySize::Percent(s));
                }
                Err(serde::de::Error::invalid_type(
                    Unexpected::Str(&s),
                    &"percent or size",
                ))
            }
        }
    }
}

fn default_empty_string_vec() -> Vec<String> {
    return vec![];
}
fn default_gptgap() -> String {
    "gpt_gap".into()
}

impl fmt::Display for ImagePartition {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut data = String::new();
        data += &format!(
            "Action: ImagePartition\nName: {}\nType: {}\nSize: {}\n",
            self.imagename, self.partitiontype, self.imagesize
        );
        data += &format!(
            "Mountpoints: {:?}\nPartitions: {:?}\n",
            self.mountpoints, self.partitions
        );
        write!(f, "{}", &data)
    }
}

impl TryFrom<&Value> for ImagePartition {
    type Error = crate::errors::Error;
    fn try_from(data: &Value) -> Result<Self, Error> {
        let run: Self = serde_yaml::from_value(data.clone()).map_err(Error::Serde)?;
        if run.mountpoints.len() != run.partitions.len() {
            return Err(Error::PartitionMountCountNotEqual);
        }
        Ok(run)
    }
}

impl ImagePartition {
    pub fn create_image(&self) -> Result<(), Error> {
        let file = File::create(&self.imagename)?;
        let res = unsafe {
            libc::fallocate(
                file.as_raw_fd(),
                libc::FALLOC_FL_ZERO_RANGE,
                0,
                self.imagesize as i64,
            )
        };

        if res < 0 {
            // Emmediatly remove file since we failed fallocate
            // TODO move to drop trait instead?
            remove_file(&self.imagename)?;
            return Err(Error::IO(std::io::Error::from_raw_os_error(res)));
        }

        Ok(())
    }

    pub fn create_partitions(&mut self) -> Result<(), Error> {
        Parted::new(&self.imagename).create_label(&self.partitiontype)?;
        for part in &mut self.partitions {
            println!("Partitioning: '{}'", part.name);
            part.run(&self.imagename)?;
            if part.name == "root" {}
        }
        Ok(())
    }

    pub fn losetup_root(&self) -> Option<Losetup> {
        for part in &self.partitions {
            if part.name == "root" {
                return Losetup::attach(&self.imagename, part.start).ok();
            }
        }
        None
    }
}

impl Runner for ImagePartition {
    fn run(&mut self) -> Result<(), Error> {
        self.create_image()?;
        self.create_partitions()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_partition_usize() {
        let yaml = r#"
---
name: ugly
fs: ext4
start: 5MiB
size: 100MiB
"#;

        let yaml: Partition = serde_yaml::from_str(&yaml).unwrap();
        assert_eq!(yaml.name, String::from("ugly"));
        assert_eq!(yaml.start, 1024 * 1024 * 5);
        assert_eq!(yaml.size, MySize::Size(1024 * 1024 * 100));
    }

    #[test]
    fn test_partition_percent_ok() {
        let yaml = r#"
---
name: ugly
fs: ext4
start: 5MiB
size: 40%
"#;

        let yaml: Partition = serde_yaml::from_str(&yaml).unwrap();
        assert_eq!(yaml.name, String::from("ugly"));
        assert_eq!(yaml.start, 1024 * 1024 * 5);
        assert_eq!(yaml.size, MySize::Percent(String::from("40%")));
    }

    #[test]
    fn test_partition_start0_ok() {
        let yaml = r#"
---
name: ugly
fs: ext4
start: 0
size: 40%
"#;

        let yaml: Result<Partition, serde_yaml::Error> = serde_yaml::from_str(&yaml);
        assert_eq!(yaml.is_ok(), true);
        let yaml = yaml.unwrap();
        assert_eq!(yaml.name, String::from("ugly"));
        assert_eq!(yaml.start, 0);
        assert_eq!(yaml.size, MySize::Percent(String::from("40%")));
    }

    #[test]
    fn test_partition_percent_parse_error() {
        let yaml = r#"
---
name: ugly
fs: ext4
start: 5MiB
size: 40%s
"#;

        let yaml: Result<Partition, serde_yaml::Error> = serde_yaml::from_str(&yaml);
        assert_eq!(true, yaml.is_err());
    }

    #[test]
    fn test_partition_percent_parse_error2() {
        let yaml = r#"
---
name: ugly
fs: ext4
start: 5MiB
size: 4r%
"#;

        let yaml: Result<Partition, serde_yaml::Error> = serde_yaml::from_str(&yaml);
        assert_eq!(true, yaml.is_err());
    }

    #[test]
    fn test_partition_percent_parse_error3() {
        let yaml = r#"
---
name: ugly
fs: ext4
start: 5MiB
size: 4000%
"#;

        let yaml: Result<Partition, serde_yaml::Error> = serde_yaml::from_str(&yaml);
        assert_eq!(true, yaml.is_err());
    }
}
