use super::runner::Runner;
use crate::errors::Error;
use crate::globals::ARGS;
use colored::Colorize;
use regex::Regex;
use serde::Deserialize;
use serde_yaml::Value;
use std::convert::TryFrom;
use std::io::prelude::*;
use std::os::unix::fs::OpenOptionsExt;
use std::path::PathBuf;
#[derive(Deserialize, Debug)]
pub struct Inject {
    file_name: PathBuf,
    #[serde(default = "def_mode")]
    file_mode: i32,
    data: String,
}

fn def_mode() -> i32 {
    0o644
}

impl TryFrom<&Value> for Inject {
    type Error = Error;
    fn try_from(value: &Value) -> Result<Self, Error> {
        let inject: Inject = serde_yaml::from_value(value.clone()).map_err(Error::Serde)?;
        if inject.file_name.is_absolute() {
            return Err(Error::AbsolutePathNotAllowed(inject.file_name));
        }
        Ok(inject)
    }
}

impl Inject {
    pub fn new(file_name: PathBuf, file_mode: i32, data: String) -> Self {
        Self {
            file_name,
            file_mode,
            data,
        }
    }

    fn inject_key_values(&mut self) -> usize {
        for (key, value) in &ARGS.map {
            // Replaces all {{KEY}} in input string with value from map.
            self.data = self.data.replace(&format!("{{{{{}}}}}", key), value);
        }

        // check if there is more left.
        let mut missing = 0;
        let re = Regex::new(r#"\{\{([A-Za-z0-9-_]+)+\}\}"#).unwrap();
        for cap in re.captures_iter(&self.data) {
            if cap.len() > 1 {
                eprintln!("{} {:?}! Make sure to add -D {}=<some_value> to command line or change file manually.",
                              "Warning! Missing define for:".cyan(), &cap[1], &cap[1]);
                missing += 1;
            }
        }

        missing
    }
}

impl Runner for Inject {
    fn run(&mut self) -> Result<(), Error> {
        //let mut data = self.data;
        self.inject_key_values();
        let mut name = ARGS.rootdir.clone();
        name.push(self.file_name.clone());
        std::fs::OpenOptions::new()
            .create(true)
            .write(true)
            .mode(self.file_mode as u32)
            .open(name)?
            .write_all(&self.data.as_bytes())
            .map_err(Error::IO)
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_run() {}
}
