use crate::errors::Error;
use std::process::Command;

pub struct Sudo {}
impl Sudo {
    pub fn new() -> Self {
        Self {}
    }

    pub fn run(&self, args: Vec<String>) -> Result<(i32, String), Error> {
        self.run_as("", args)
    }

    pub fn run_as(&self, login: &str, mut args: Vec<String>) -> Result<(i32, String), Error> {
        if !login.is_empty() {
            args.insert(0, String::from(login));
            args.insert(0, String::from("-u"));
        }

        let output = Command::new("sudo")
            .args(args)
            .env("LC_ALL", "C")
            .output()
            .expect(&"Sudo failed");

        if !output.status.success() {
            return Err(Error::CommandFailed(
                format!("{:?}", output),
                output.status.code().unwrap(),
            ));
        }
        let stdout = String::from_utf8_lossy(&output.stdout);
        Ok((output.status.code().unwrap(), stdout.to_string()))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_sudo() {
        let login = std::env::var("USER").unwrap();
        let sudo = Sudo::new();
        let mut args = vec![];
        args.push(String::from("echo"));
        args.push(format!("print hello world as {}", login));
        let res = sudo.run_as(&login, args);
        assert_eq!(true, res.is_ok());
        let res = res.unwrap();
        assert_eq!(
            format!("print hello world as {}\n", login),
            res.1
        );
    }
}
