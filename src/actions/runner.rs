pub trait Runner {
    fn run(&mut self) -> Result<(), crate::errors::Error>;
}
