use crate::errors::Error;
use std::path::PathBuf;
use std::process::{Command, Stdio};
#[derive(Debug)]
pub struct ChRoot {
    path: PathBuf,
}

impl ChRoot {
    pub fn new(path: &PathBuf) -> Self {
        Self { path: path.clone() }
    }

    pub fn run(&self, command: String) -> Result<(), Error> {
        self.run_as("", command)
    }

    pub fn run_as(&self, user: &str, command: String) -> Result<(), Error> {
        let mut args: Vec<String> = Vec::new();
        args.push(String::from("chroot"));
        args.push(String::from(self.path.to_string_lossy()));
        if !user.is_empty() {
            args.push(String::from("su"));
            args.push(String::from(user));
        }
        args.push(String::from("/bin/sh"));
        args.push(String::from("-c"));
        args.push(command.clone());
        let mut child = Command::new("sudo")
            .args(args)
            .stdin(Stdio::piped())
            .env("DEBIAN_FRONTEND", "noninteractive")
            .env("DEBCONF_NONINTERACTIVE_SEEN", "true")
            .env("LC_ALL", "C")
            .spawn()
            .unwrap_or_else(|_| panic!("'{}' failed", command));

        let exitstatus = child.wait();
        if let Ok(exitstatus) = exitstatus {
            if !exitstatus.success() {
                return Err(Error::CommandFailed(
                    command,
                    exitstatus.code().unwrap_or(0),
                ));
            }
            Ok(())
        } else {
            panic!("WTF should not reach this");
        }
    }
}
