use super::runner::Runner;
use crate::errors::Error;
use crate::globals::{ARGS, CHROOT};
use serde_derive::Deserialize;
use serde_yaml::Value;
use std::convert::TryFrom;
use std::process::{Command, Stdio};
#[derive(Deserialize, Debug)]
pub struct Debootstrap {
    #[serde(default)]
    arch: String,
    action: String,
    exclude: Vec<String>,
    include: Vec<String>,
    #[serde(default)]
    suite: String,
    components: Vec<String>,
    #[serde(default)]
    mirror: String,
    #[serde(default)]
    //FIXME check this can only be minbase,buildd,fakeroot
    variant: String,
    #[serde(default)]
    check_pkg: bool,
}

impl Default for Debootstrap {
    fn default() -> Self {
        Debootstrap {
            arch: String::new(),
            action: String::from("debootstrap"),
            suite: String::from("stretch"),
            components: vec![],
            include: vec![],
            exclude: vec![],
            mirror: String::new(),
            variant: String::from("minbase"),
            check_pkg: true,
        }
    }
}

impl Runner for Debootstrap {
    fn run(&mut self) -> Result<(), Error> {
        self.stage1()?;
        self.stage2()
    }
}

impl TryFrom<&Value> for Debootstrap {
    type Error = serde_yaml::Error;
    fn try_from(data: &Value) -> Result<Self, serde_yaml::Error> {
        serde_yaml::from_value(data.clone())
    }
}

impl Debootstrap {
    pub fn set_arch(&mut self, arch: String) {
        self.arch = arch;
    }

    fn stage1(&self) -> Result<(), Error> {
        let mut args: Vec<String> = Vec::new();
        args.push(String::from("debootstrap"));
        args.push(String::from("--foreign"));
        args.push(String::from("--no-merged-usr"));
        args.push(format!("--arch={}", self.arch));
        args.push(format!("--variant={}", self.variant));
        let comp = self.components.join(",");
        if !comp.is_empty() {
            args.push(format!("--components={}", comp));
        }

        let includes = self.include.join(",");
        if !includes.is_empty() {
            args.push(format!("--include={}", includes));
        }

        let excludes = self.exclude.join(",");
        if !excludes.is_empty() {
            args.push(format!("--exclude={}", excludes));
        }

        args.push(self.suite.clone());
        args.push(ARGS.rootdir.to_string_lossy().into());
        if !self.mirror.is_empty() {
            args.push(self.mirror.clone());
        }
        let mut child = Command::new("sudo")
            .args(&args)
            .stdin(Stdio::piped())
            .spawn()
            .unwrap_or_else(|e| panic!("Debootstrap failed using args: {:?} cause {}", &args, e));

        let exitcode = child.wait().unwrap_or_else(|e| {
            panic!(
                "Execute of debootstrap failed using arguments: {:?} cause {}",
                &args, e
            )
        });
        if !exitcode.success() {
            return Err(Error::CommandFailed(
                String::from("Deboostrap failed"),
                exitcode.code().unwrap_or(0),
            ));
            //panic!("Debootstrap failed with using args: {:?} exit code was: {}", &args, exitcode.code().unwrap());
        }
        Ok(())
    }

    fn stage2(&self) -> Result<(), Error> {
        CHROOT.run("/debootstrap/debootstrap --second-stage".to_string())
    }
}
