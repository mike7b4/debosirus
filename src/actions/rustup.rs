use super::chroot::ChRoot;
use super::runner::Runner;
use crate::errors::Error;
use crate::globals::ARGS;
use serde::Deserialize;
use serde_yaml::Value;
use std::convert::TryFrom;
#[derive(Deserialize, Debug)]
pub struct Rustup {
    login: String,
    targets: Option<Vec<String>>,
}

impl TryFrom<&Value> for Rustup {
    type Error = Error;
    fn try_from(data: &Value) -> Result<Self, Error> {
        let rustup: Rustup =
            serde_yaml::from_value(data.clone()).map_err(Error::Serde)?;
        Ok(rustup)
    }
}

impl Runner for Rustup {
    fn run(&mut self) -> Result<(), Error> {
        let chroot = ChRoot::new(&ARGS.rootdir);
        chroot.run_as(
            &self.login,
            "curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh".into(),
        )?;
        if let Some(ref targets) = &self.targets {
            for target in targets {
                chroot.run_as(&self.login, format!("rustup target install {}", target))?
            }
        }
        Ok(())
    }
}
