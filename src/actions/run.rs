use super::chroot::ChRoot;
use super::runner::Runner;
use crate::errors::Error;
use crate::globals::ARGS;
use serde::Deserialize;
use serde_yaml::Value;
use std::convert::TryFrom;
#[derive(Deserialize, Debug)]
pub struct Run {
    #[serde(default = "default_true")]
    chroot: bool,
    command: Option<String>,
    commands: Option<Vec<String>>,
}

fn default_true() -> bool {
    true
}

impl TryFrom<&Value> for Run {
    type Error = Error;
    fn try_from(data: &Value) -> Result<Self, Error> {
        // Fixme use map in some way...
        let run: Run = match serde_yaml::from_value(data.clone()) {
            Ok(run) => run,
            Err(err) => {
                return Err(Error::Serde(err));
            }
        };
        if run.command.is_none() && run.commands.is_none() {
            return Err(Error::MissingField("command(s)"));
        }
        if !run.chroot {
            return Err(Error::NotImplemented("run without chroot".into()));
        }
        Ok(run)
    }
}

impl Runner for Run {
    fn run(&mut self) -> Result<(), Error> {
        if self.chroot {
            let chroot = ChRoot::new(&ARGS.rootdir);
            if let Some(ref command) = &self.command {
                chroot.run(command.clone())?
            }
            if let Some(ref commands) = &self.commands {
                for command in commands {
                    chroot.run(command.clone())?
                }
            }
        } else {
            // should not come here since we already filter it in tryfrom
            panic!("Not implemented");
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::actions::run::Run;
    use std::convert::TryFrom;
    #[test]
    fn test_yaml_commands_ok() {
        let yaml = r#"
chroot: true
commands: ["apa"]
"#;
        let yaml = serde_yaml::from_str(&yaml);
        assert_eq!(yaml.is_ok(), true);
        assert_eq!(Run::try_from(&yaml.unwrap()).is_ok(), true);
    }

    #[test]
    fn test_yaml_command_ok() {
        let yaml = r#"
chroot: true
command: "apa"
"#;
        let yaml = serde_yaml::from_str(&yaml);
        assert_eq!(yaml.is_ok(), true);
        assert_eq!(Run::try_from(&yaml.unwrap()).is_ok(), true);
    }

    #[test]
    fn test_yaml_missing_command() {
        let yaml = r#"
chroot: true
"#;
        let yaml = serde_yaml::from_str(&yaml);
        assert_eq!(yaml.is_ok(), true);
        assert_eq!(Run::try_from(&yaml.unwrap()).is_err(), true);
    }

    #[test]
    fn test_yaml_non_chroot_not_impl() {
        let yaml = r#"
chroot: false
command: apa
"#;
        let yaml = serde_yaml::from_str(&yaml);
        assert_eq!(yaml.is_ok(), true);
        assert_eq!(Run::try_from(&yaml.unwrap()).is_err(), true);
    }
}
