use super::chroot::ChRoot;
use super::runner::Runner;
use crate::errors::Error;
use crate::globals::ARGS;
use serde_derive::Deserialize;
use serde_yaml::Value;
use std::convert::TryFrom;
#[derive(Deserialize, Debug, Default)]
pub struct Apt {
    pub action: String,
    pub packages: Vec<String>,
}

impl TryFrom<&Value> for Apt {
    type Error = Error;
    fn try_from(data: &Value) -> Result<Self, Error> {
        serde_yaml::from_value(data.clone()).map_err(Error::Serde)
    }
}

impl Runner for Apt {
    fn run(&mut self) -> Result<(), crate::errors::Error> {
        let chroot = ChRoot::new(&ARGS.rootdir);
        chroot.run(format!(
            "apt update && /usr/bin/apt install --allow-unauthenticated -y {}",
            self.packages.join(" ")
        ))
    }
}
