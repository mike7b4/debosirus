use crate::actions::sudo::Sudo;
use crate::errors::Error;
use std::path::PathBuf;
use std::process::Command;
#[derive(Debug)]
pub struct Losetup {
    pub path: String,
}

impl Losetup {
    fn request_loopdevice(&self) -> Result<String, Error> {
        let output = Command::new("losetup")
            .arg("-f")
            .env("LC_ALL", "C")
            .output()
            .expect(&"Sudo failed");

        let stdout = String::from_utf8_lossy(&output.stdout);
        if !output.status.success() {
            return Err(Error::CommandFailed(
                format!("{:?}", output),
                output.status.code().unwrap(),
            ));
        }

        let loopdevice = stdout.split('\n').next().unwrap();
        if !loopdevice.starts_with("/dev/loop") {
            panic!("losetup parse error");
        }
        Ok(loopdevice.to_string())
    }

    pub fn attach(imagefile: &str, offset: usize) -> Result<Self, Error> {
        let mut lo = Self {
            path: String::new(),
        };
        let loopdevice = lo.request_loopdevice()?;
        let mut args = Vec::new();
        args.push(String::from("losetup"));
        args.push(loopdevice.clone());
        args.push(imagefile.into());
        args.push(String::from("-o"));
        args.push(offset.to_string());
        println!("losetup {} {} -o {}", &loopdevice, &imagefile, offset);
        Sudo::new().run(args)?;
        lo.path = loopdevice;

        Ok(lo)
    }

    fn detach(&mut self) {
        if self.path.is_empty() {
            return;
        }
        println!("Detach lo {:?}", self.path);
        let mut args = Vec::new();
        args.push(String::from("losetup"));
        args.push(String::from("-d"));
        args.push(self.path.clone());
        Sudo::new()
            .run(args)
            .unwrap_or_else(|e| panic!("Could not detach lo got {} '{}'", e, self.path.clone()));
        self.path = String::new();
    }
}

impl Drop for Losetup {
    fn drop(&mut self) {
        self.detach();
    }
}

pub struct Mount {
    path: PathBuf,
}

impl Mount {
    pub fn mount(lodev: Losetup, path: &PathBuf) -> Option<Self> {
        let mut args = Vec::new();
        args.push(String::from("mount"));
        args.push(String::from("-o"));
        args.push(String::from("exec,dev"));
        args.push(lodev.path.clone());
        args.push(path.clone().into_os_string().into_string().unwrap());
        Sudo::new().run(args).unwrap_or_else(|e| {
            panic!("Could not mount: '{}' cause {}", path.to_string_lossy(), e)
        });
        Some(Self { path: path.clone() })
    }

    pub fn umount(&self) {
        let mut args = Vec::new();
        args.push(String::from("umount"));
        args.push(self.path.clone().into_os_string().into_string().unwrap());
        Sudo::new().run(args).unwrap_or_else(|e| {
            panic!(
                "Could not umount: '{}' cause {}",
                self.path.to_string_lossy(),
                e
            )
        });
        println!("umounted: '{}'", self.path.to_string_lossy());
    }
}

impl Drop for Mount {
    fn drop(&mut self) {
        self.umount();
    }
}

#[cfg(test)]
mod tests {
    use super::Losetup;
    #[test]
    fn test_request_loopdevice() {
        // This is kind of dirty what we do is request losetup twice
        // then compare if same
        let losetup = Losetup {
            path: String::new(),
        }
        .request_loopdevice();
        let losetup2 = Losetup {
            path: String::new(),
        }
        .request_loopdevice();
        assert_eq!(true, losetup.is_ok());
        assert_eq!(true, losetup2.is_ok());
        let losetup = losetup.unwrap();
        let losetup2 = losetup2.unwrap();
        assert_eq!(true, losetup.starts_with(&"/dev/loop"));
        // In theory this may race since we ask OS twice
        // and we may get different results but unlikely
        // TODO mock instead
        assert_eq!(losetup, losetup2);
    }
}
