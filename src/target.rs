use serde::Deserialize;
use serde_yaml::Value;
use std::convert::TryFrom;
#[derive(Deserialize, Debug, Default)]
pub struct Target {
    name: String,
    pub architecture: String,
    #[serde(default)]
    toolchain: String,
    #[serde(default)]
    version: String,
}

impl TryFrom<&Value> for Target {
    type Error = serde_yaml::Error;
    fn try_from(data: &Value) -> Result<Self, serde_yaml::Error> {
        serde_yaml::from_value(data.clone())
    }
}
