use std::fmt;
#[derive(Debug)]
pub enum Error {
    CommandExecuteFailed,
    CommandFailed(String, i32),
    Serde(serde_yaml::Error),
    IO(std::io::Error),
    NotImplemented(String),
    PartitionMountCountNotEqual,
    MkLabel,
    MissingField(&'static str),
    MissingDefine(&'static str),
    AbsolutePathNotAllowed(std::path::PathBuf),
    Mount,
}

impl From<std::io::Error> for Error {
    fn from(io: std::io::Error) -> Self {
        Error::IO(io)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &*self {
            Error::CommandExecuteFailed => write!(f, "Command execute failed"),
            Error::CommandFailed(cmd, exitcode) => {
                write!(f, "Command: '{}' failed with exitcode {}", cmd, exitcode)
            }
            Error::Serde(e) => write!(f, "Failed to parse recipe got error: {}", e),
            Error::IO(e) => write!(f, "File or path error got: {}", e),
            Error::NotImplemented(action) => write!(f, "Action: '{}' is not implemented!", &action),
            Error::PartitionMountCountNotEqual => write!(f, "Partition count missmatch"),
            Error::MkLabel => write!(f, "MkLabel error!"),
            Error::MissingField(field) => write!(f, "Missing field: {}", field),
            Error::MissingDefine(key) => {
                write!(f, "Missing define: {0} please add -D{0}=<value>", key)
            }
            Error::AbsolutePathNotAllowed(path) => {
                write!(f, "Absolute paths is not allowed! '{:?}'", path)
            }
            Error::Mount => write!(f, "Mount failed"),
        }
    }
}
