#[macro_use]
extern crate lazy_static;
mod actions;
mod arguments;
mod errors;
mod globals;
mod losetup;
mod target;
use self::target::Target;
use crate::errors::Error;
use actions::apt::Apt;
use actions::debootstrap::Debootstrap;
use actions::image_partition::ImagePartition;
use actions::inject::Inject;
use actions::run::Run;
use actions::runner::Runner;
use actions::rustup::Rustup;
use actions::user::User;
use colored::*;
use globals::ARGS;
use losetup::Mount;
use serde_yaml::Value;
use std::convert::TryFrom;
use std::fmt;
use std::fs::File;
use std::io::Read;
use std::vec::Vec;

#[derive(Debug)]
enum Actions {
    Debootstrap(Debootstrap),
    Apt(Apt),
    Run(Run),
    ImagePartition(ImagePartition),
    User(User),
    Inject(Inject),
    Rustup(Rustup),
}

impl fmt::Display for Actions {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Actions::Debootstrap(_) => write!(f, "Deboostrap"),
            Actions::Apt(_) => write!(f, "Apt"),
            Actions::ImagePartition(_) => write!(f, "ImagePartition"),
            Actions::Rustup(_) => write!(f, "Install rust"),
            e => write!(f, "{:?}", e),
        }
    }
}

fn add_action(
    arch: &str,
    name: &str,
    action: &Value,
    action_list: &mut Vec<Actions>,
) -> Result<(), Error> {
    match &*name {
        "debootstrap" => {
            let mut debootstrap =
                Debootstrap::try_from(action).expect("Debootstrap has wrong settings");
            debootstrap.set_arch(arch.into());
            action_list.push(Actions::Debootstrap(debootstrap));
        }
        "apt" => {
            let apt = Apt::try_from(action)?;
            if !apt.packages.is_empty() {
                action_list.push(Actions::Apt(apt));
            } else {
                eprintln!(
                    "{}",
                    "Ignored action: 'apt' since no packages was given.".red()
                );
            }
        }
        "user" => {
            let user = User::try_from(action)?;
            action_list.push(Actions::User(user));
        }
        "run" => {
            let run = Run::try_from(action)?;
            action_list.push(Actions::Run(run));
        }
        "image-partition" => {
            let part = ImagePartition::try_from(action)?;
            action_list.push(Actions::ImagePartition(part));
        }
        "inject" => {
            action_list.push(Actions::Inject(Inject::try_from(action)?));
        }
        "rustup" => {
            action_list.push(Actions::Rustup(Rustup::try_from(action)?));
        }
        // FIXME
        _ => eprintln!("{}", Error::NotImplemented(name.to_string())),
    }

    Ok(())
}

fn main() -> Result<(), usize> {
    let mut action_list: Vec<Actions> = Vec::new();

    // when mount active drop trait will umount when we go out of scope
    let mut _mountpoint: Option<Mount> = None;
    println!("{:?}", *ARGS);

    let mut file = File::open(&*ARGS.config).expect("Could not open recipe");
    let mut data = String::new();
    match file.read_to_string(&mut data) {
        Ok(_) => {}
        Err(e) => {
            println!(
                "Could not read recipe in {}, {}",
                &*ARGS.config.to_string_lossy(),
                e
            );
            return Err(1);
        }
    }
    let deboot: Value = serde_yaml::from_str(&data).map_err(|e| {
        eprintln!("{}", e);
        1 as usize
    })?;
    let mut target = Target::default();

    let deboot = deboot.as_mapping().unwrap();
    for (s, m) in deboot {
        match s.as_str().unwrap() {
            "target" => {
                target = Target::try_from(m).unwrap();
            }
            "actions" => {
                let empty = vec![];
                let actions = m.as_sequence().unwrap_or(&empty);
                for action in actions {
                    let map = action.as_mapping().expect("Action");
                    let def = Value::from(String::new());
                    let name = map
                        .get(&Value::from("action".to_string()))
                        .unwrap_or(&def)
                        .as_str()
                        .unwrap();
                    if ARGS.is_action_skipped(&name) {
                        println!("Skipping action: {}", &name);
                        continue;
                    }

                    if let Err(err) =
                        add_action(&target.architecture, &name, &action, &mut action_list)
                    {
                        eprintln!("{}", err);
                        std::process::exit(1);
                    }
                }
            }
            _ => {}
        };
    }

    for action in &mut action_list {
        println!("Current action: '{}'", action);
        let res: Result<(), Error> = match action {
            Actions::ImagePartition(action) => match action.run() {
                Ok(()) => {
                    let _mount = Mount::mount(action.losetup_root().unwrap(), &ARGS.rootdir)
                        .ok_or(Error::Mount);
                    Ok(())
                }
                Err(err) => Err(err),
            },
            Actions::Debootstrap(action) => action.run(),
            Actions::Apt(action) => action.run(),
            Actions::User(action) => action.run(),
            Actions::Run(action) => action.run(),
            Actions::Inject(action) => action.run(),
            Actions::Rustup(action) => action.run(),
        };

        if let Err(err) = res {
            eprintln!("Action: '{}' failed with: {}", action, err);
            break;
        }
    }

    Ok(())
}
