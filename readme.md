# Debosirus

A Rust application to make it easy to create Debian OS based image from Yaml input text file.

This is experimental and heavily based on debos (golang) yaml recipe format.

I just done this for fun and to learn Rust.
Feel free to contibute and improve and/or fork.

# Deb dependies

  * Deboostrap
  * kpartx/losetup
  * mkfs.XXX

# Todo

  * Better error handling it should not always 'panic'. Some actions should still continue.

# Example usage

Notice! 10Gb disk space needed by the example.

```cargo run -- --recipe example/librem5-builder.yaml --rootdir ./mychroot```

After creating of above image you can mount it. Make sure you also have qemu-aarch64-static installed on you machine or else you can't compile or run anything inside the builder.

```
   sudo kpartx -a librem5-builder.img
   # replace XXX with the output given from kpart command above.
   sudo mount /dev/mapper/loopXXX ./mychroot
   # chroot to it:
   sudo chroot mychroot
   # recommend drop privileges inside chroot
   su purism
   cd ~
   git clone https://source.puri.sm/Librem5/phosh.git
   cd phosh
   # check readme howto build phosh
```

