#!/bin/bash -eu
cmd="${1:?Expect arg --mount or --umount}"
chroot_dir="${2:?Expect dir where to mount}"
img="${3:?Expect an image}"
if [ "$cmd" = "--mount" ]; then
	mkdir -p "$chroot_dir"
	if [ -x "$chroot_dir/home" ]; then
		echo -e "\e[31;1mError:\e[0m The '$img' is already mounted in '$chroot_dir'."
		exit 1
	fi
	part=$(kpartx -va "$img")
	read -ra part <<< "$part"
	mount /dev/mapper/${part[2]} "$chroot_dir"
	mkdir -p "$chroot_dir"/home/purism/src
	mount --bind ../.. "$chroot_dir"/home/purism/src
	echo -e "\e[34;1mOK\e[0m mounted: '$img' in: '$chroot_dir'."
elif [ "$cmd" = "--umount" ]; then
	umount "$chroot_dir"/home/purism/src || true
	umount "$chroot_dir" || true
	kpartx -d "$img"
	echo -e "'\e[34;1mOK\e[0m '$img' has been umounted from '$chroot_dir'."
elif [ "$cmd" = "--chroot" ]; then
	chroot "$chroot_dir" /bin/bash -c "su -"
else
	echo "First arg is --mount or --umount"
	echo "Second is chroot dir to mount (default 'mychroot')"
	echo "Third argument is image name (default to 'librem-builder.img')"
	[ "$cmd" = "--help" ]
fi
